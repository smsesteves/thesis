\select@language {portuges}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Apresenta\IeC {\c c}\IeC {\~a}o da Indra}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Enquadramento}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Problema e Caracteriza\IeC {\c c}\IeC {\~a}o}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Motiva\IeC {\c c}\IeC {\~a}o e Objetivos}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Benef\IeC {\'\i }cios da Solu\IeC {\c c}\IeC {\~a}o}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Estrutura}{3}{section.1.6}
\contentsline {chapter}{\numberline {2}Enquadramento Te\IeC {\'o}rico}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Sistemas de Informa\IeC {\c c}\IeC {\~a}o}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Sistemas de \textit {Business Intelligence} }{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Conceito de Data Warehouse}{9}{subsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.1.1}Metodologia Inmon}{9}{subsubsection.2.2.1.1}
\contentsline {subsubsection}{\numberline {2.2.1.2}Metodologia Kimball}{10}{subsubsection.2.2.1.2}
\contentsline {subsubsection}{\numberline {2.2.1.3}Metodologia Inmon vs Metodologia Kimball}{10}{subsubsection.2.2.1.3}
\contentsline {subsubsection}{\numberline {2.2.1.4}Modela\IeC {\c c}\IeC {\~a}o entidade-rela\IeC {\c c}\IeC {\~a}o}{10}{subsubsection.2.2.1.4}
\contentsline {subsubsection}{\numberline {2.2.1.5}Modela\IeC {\c c}\IeC {\~a}o dimensional}{11}{subsubsection.2.2.1.5}
\contentsline {subsection}{\numberline {2.2.2}Arquitetura de um Data Warehouse}{12}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}Sistemas Fonte Operacionais}{13}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}\textit {Data Staging Area} }{13}{subsubsection.2.2.2.2}
\contentsline {paragraph}{\textit {Processing}}{13}{section*.15}
\contentsline {paragraph}{\textit {Data Store} }{14}{section*.16}
\contentsline {subsubsection}{\numberline {2.2.2.3}\textit {Data Presentation Area} }{14}{subsubsection.2.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.2.4}Ferramentas de acesso aos dados}{14}{subsubsection.2.2.2.4}
\contentsline {section}{\numberline {2.3}\IeC {\^A}mbito da solu\IeC {\c c}\IeC {\~a}o}{14}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Gest\IeC {\~a}o Acad\IeC {\'e}mica}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Projetos Existentes}{15}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Solu\IeC {\c c}\IeC {\~a}o Atual - iBISmartGiaf}{16}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Tecnologias}{17}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}Requisitos e Funcionalidades}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Requisitos Funcionais}{21}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Procura do Ciclo de Estudos}{21}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Efic\IeC {\'a}cia Formativa e Resultados dos Estudantes}{22}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Receita}{22}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Custos}{23}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}Requisitos T\IeC {\'e}cnicos}{23}{section.3.2}
\contentsline {chapter}{\numberline {4}Arquitetura}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Sistemas Fonte Operacionais}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Concurso Nacional de Acesso}{26}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Inqu\IeC {\'e}ritos RAIDES}{26}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Vagas}{27}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Sistema de inform\IeC {\c c}\IeC {\~a}o Gest\IeC {\~a}o Acad\IeC {\'e}mica}{27}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}\textit {ERP} - GIAF}{28}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}\textit {Data Staging Area} }{28}{section.4.2}
\contentsline {section}{\numberline {4.3}\textit {Data Presentation Area} }{28}{section.4.3}
\contentsline {section}{\numberline {4.4}Ferramentas de acesso aos dados}{28}{section.4.4}
\contentsline {chapter}{\numberline {5}Implementa\IeC {\c c}\IeC {\~a}o}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}An\IeC {\'a}lise, Desenho e Levantamento Funcional}{30}{section.5.1}
\contentsline {section}{\numberline {5.2}Desenvolvimento e Implementa\IeC {\c c}\IeC {\~a}o}{31}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Sistemas Fonte Operacionais}{31}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\textit {Data Staging Area} }{32}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}\textit {Processing}}{32}{subsubsection.5.2.2.1}
\contentsline {paragraph}{Dimens\IeC {\~a}o}{32}{section*.26}
\contentsline {paragraph}{Facto}{33}{section*.28}
\contentsline {subsubsection}{\numberline {5.2.2.2}\textit {Data Store} }{35}{subsubsection.5.2.2.2}
\contentsline {subsection}{\numberline {5.2.3}\textit {Data Presentation Area} }{35}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}Camada F\IeC {\'\i }sica}{36}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}Camada de Neg\IeC {\'o}cio}{36}{subsubsection.5.2.3.2}
\contentsline {subsubsection}{\numberline {5.2.3.3}Camada de Apresenta\IeC {\c c}\IeC {\~a}o}{37}{subsubsection.5.2.3.3}
\contentsline {paragraph}{Dimens\IeC {\~a}o}{37}{section*.34}
\contentsline {paragraph}{M\IeC {\'e}trica / Indicador}{38}{section*.36}
\contentsline {paragraph}{Base de dados anal\IeC {\'\i }tica}{40}{section*.39}
\contentsline {subsubsection}{\numberline {5.2.3.4}Ferramentas de acesso aos dados}{40}{subsubsection.5.2.3.4}
\contentsline {paragraph}{\textit {Sharepoint}}{41}{section*.40}
\contentsline {paragraph}{\textit {Power BI}}{41}{section*.41}
\contentsline {section}{\numberline {5.3}Forma\IeC {\c c}\IeC {\~a}o / Documenta\IeC {\c c}\IeC {\~a}o}{41}{section.5.3}
\contentsline {section}{\numberline {5.4}Testes de Aceita\IeC {\c c}\IeC {\~a}o}{41}{section.5.4}
\contentsline {section}{\numberline {5.5}Entrada em Produ\IeC {\c c}\IeC {\~a}o}{42}{section.5.5}
\contentsline {chapter}{\numberline {6}Resultados}{45}{chapter.6}
\contentsline {section}{\numberline {6.1}Opera\IeC {\c c}\IeC {\~a}o e execu\IeC {\c c}\IeC {\~a}o}{45}{section.6.1}
\contentsline {section}{\numberline {6.2}Grau de corre\IeC {\c c}\IeC {\~a}o e utilidade dos resultados}{45}{section.6.2}
\contentsline {section}{\numberline {6.3}Interfaces e exemplos}{46}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Plataforma Colaborativa - \textit {Sharepoint}}{47}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}\textit {Power BI}}{48}{subsection.6.3.2}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~o}es e Trabalho Futuro}{49}{chapter.7}
\contentsline {section}{\numberline {7.1}Discuss\IeC {\~a}o dos Resultados}{49}{section.7.1}
\contentsline {section}{\numberline {7.2}Conclus\IeC {\~o}es}{50}{section.7.2}
\contentsline {section}{\numberline {7.3}Satisfa\IeC {\c c}\IeC {\~a}o dos Objetivos}{51}{section.7.3}
\contentsline {section}{\numberline {7.4}Benef\IeC {\'\i }cios verificados}{51}{section.7.4}
\contentsline {section}{\numberline {7.5}Evolu\IeC {\c c}\IeC {\~a}o futura}{52}{section.7.5}
\contentsline {chapter}{Refer\^{e}ncias}{53}{section*.51}
