\chapter{Implementação} \label{chap:5}


Não existe qualquer restrição da área onde um projeto de \textit{BI} pode ser implementado. Segundo Rasmussen ~\cite{Rasmussen}, a implementação de um projeto pode ser dividido em cinco fases distintas:

\begin{itemize}
  \item Levantamento de requisitos - Nesta fase é expectável que seja feita uma recolha e discussão dos requisitos com o cliente, identificando as funcionalidades chave para o sucesso da solução;
  \item Desenho das soluções - A fase de desenho da solução incorpora o planeamento inicial, definição do conjunto de \textit{interfaces} que a solução deve disponibilizar bem como os perfis de utilização. A arquitetura da solução de \textit{BI} é também definida e desenhada nesta fase;
  \item Implementação e validação - Esta etapa tende a ser a mais longa de todas as etapas de implementação da solução. Durante esta fase todos os componentes da arquitetura da solução são implementados e são integrados na solução. As ferramentas de suporte à solução são também idealizadas e desenvolvidas;
  \item Disponibilização da solução - Nesta fase, a solução é disponibilizada aos utilizadores finais em conjunto com os manuais de apoio e formação. Por vezes, esta etapa é também acompanhada de formações presenciais com a equipa de desenvolvimento e os respetivos utilizadores finais.
  \item Suporte e manutenção - Etapa que se prolonga ao logo do tempo e sempre que se exija a necessidade de melhoramentos, revisões ou manutenção da solução.
\end{itemize}


Cada uma destas fases foi suportada e enquadrada na metodologia oficial da Indra, MIDAS (Método Indra para o Desenvolvimento, Adaptação e Serviço). 

A metodologia apresentada, MIDAS, é a metodologia oficial da empresa para a construção e desenvolvimento das operações e utiliza-se de forma combinada com a metodologia MIGP (Método Indra para Gestão Projetos) também representada na figura seguinte. Esta metodologia é responsável pela Gestão do Projeto e está presente durante todas as fases de desenvolvimento. 

MIDAS em conjunto com MIGP indicam que, no final de cada fase de desenvolvimento da solução, devemos ter um \textit{deliverable} que resuma o trabalho efetuado durante a fase de implementação que está a cessar.

De salientar que o método proposto por Rasmussen vai de encontro ao MIDAS em conjunto com MIGP que foram utilizados no decorrer deste projeto.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{midasmigp}
    \caption{Metodologia MIDAS e Metodologia MIGP}
    \label{fig:arquitecture}
  \end{center}
\end{figure}


No sentido de alcançar os objetivos propostos, a implementação desta solução decorreu segundo a proposta acima descrita. A proposta sugerida por Rasmussen em conjunto com as metodologia oficiais da Indra resultaram nas seguintes fases de desenvolvimento da solução.


\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{gestaoproj}
    \caption{Fases de desenvolvimento da Solução}
    \label{fig:arquitecture}
  \end{center}
\end{figure}



\section{Análise, Desenho e Levantamento Funcional}


A primeira ação a ser tomada durante esta primeira fase é planear a execução de todo o processo. As diferentes etapas de implementação devem ser identificadas, estimada uma duração para a sua implementação e analisar as precedências de outras tarefas, se estas existirem. A figura que se segue ilustra o planeamento para a execução do projeto em questão.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{plan}
    \caption{Plano de Trabalhos}
    \label{fig:plano}
  \end{center}
\end{figure}

Durante esta fase é expectável terminar o levantamento funcional de todos os indicadores que sejam necessários para as análises pretendidas e ainda identificar os diversos sistemas fontes que os providenciam. 
O desenho da arquitetura e a interação dos diferentes componentes são também idealizados nesta fase, recorrendo a uma análise crítica, ponderando todos os mecanismos que estejam à disposição.
O levantamento efetuado foi descrito num ficheiro representado no anexo ~\ref{sec:anexoa}, que é denominado de Modelo de Informação de Gestão. Este ficheiro é o \textit{deliverable} que deve resultar no final desta primeira fase.
Neste documento apresentam-se todos os indicadores que devem ser implementados e as suas dimensões de análise, incluindo a fórmula de cálculo e sistema fonte de dados, se necessário.

\section{Desenvolvimento e Implementação}

A fase de implementação da solução é a fase mais extensa e necessita de ser planeada e estimada de uma forma rigorosa, pois, estes projetos são encarados com prazos rigorosos de planeamento, execução e implementação. Esta etapa, como já foi referido anteriormente, prende-se com a construção do \textit{DW}. 
A estruturação do \textit{DW}, em conjunto com os respetivos processos de integração e as plataformas de exploração da informação representam o \textit{deliverable} esperado no final desta segunda fase de implementação. Antes de avançar para as etapas seguintes de Formação e Testes, deve ser feito um carregamento completo e exaustivo da informação presente nos diversos sistemas fonte.


\subsection{Sistemas Fonte Operacionais}

A primeira etapa para a construção do \textit{DW} é a identificação dos sistemas fonte e a sua integração com a solução. Para o projeto em causa, os sistemas fonte necessários são:

\begin{itemize}
	\item{Concurso Nacional de Acesso}
	\item{Inquéritos RAIDES}
	\item{Vagas}
	\item{Sistema de informção Gestão Académica}
	\item{\textit{ERP} - GIAF}
\end{itemize}

A integração destes sistemas fonte é feita através de duas tecnologias: a primeira denominada de \textit{Linked Servers}, presentes no \textit{Microsoft SQL Server}, permite que duas base de dados distintas, em ambientes também eles distintos consigam comunicar. Assim foi possível integrar o sistema de informação de apoio à gestão académica e as bases de dados do \textit{ERP}.
Os restantes sistemas fonte, como não representam instâncias físicas de base de dados mas sim ficheiros, necessitam de uma integração distinta da anterior. A ferramenta \textit{Microsoft Integration Services} disponibiliza a possibilidade de integrar estes ficheiros nas base de dados da solução através da criação de um \textit{package}. Um \textit{package} é constituído por um conjunto de componentes, ligados através de um \textit{workflow} e que pode ser executado remotamente. 

\subsection{\textit{Data Staging Area} }

A segunda etapa para a construção do \textit{DW} tem como principal objetivo a criação dos \textit{Data Marts}.
Devido à heterogeniedade dos diversos sistemas fonte, os processos de carregamento da informação foram desenvolvidos para primeiro carregar a informação para uma \textit{staging}, com uma estrutura definida e controlada para evitar demasiadas operações de transformação de dados e incoerências. Apenas os dados que correspondam à estrutura definida na \textit{staging} são carregados para a \textit{Data Store}. Esta estratégia revelou-se bastante importante pois, devido principalmente aos ficheiros de dados serem facilmente manipuláveis, e consequência disso, muito susceptíveis a erros humanos, tornou o carregamento da informação mais fiável.

\subsubsection{\textit{Processing}}

A etapa de processamentos dos dados representa a extração, transformação e carregamentos dos dados de cada sistema fonte identificado. Este processamento corre através de um conjunto de processos de \textit{ETL} que, quando executados em conjunto e na ordem correta, carregam os dados nas tabelas da \textit{Data Store} . Existem dois tipos de carregamentos distintos consoante a tabela final seja uma tabela de dimensão ou uma tabela de facto.
A execução dos procedimentos de \textit{ETL} fazem também eles parte do \textit{workflow} do \textit{package} de integração de cada sistema fonte corresponde. O principal objetivo de manter o \textit{workflow} completo com o carregamento da \textit{staging} e da \textit{Data Store} é evitar que sejam executados os processos sobre a \textit{Data Store}, enquanto os dados ainda não estão recolhidos das respetivas fontes.

\paragraph{Dimensão} Quando o processo de carregamento tem como destino uma tabela de dimensão da \textit{Data Store}, este processo é responsável por atualizar os dados que já existirem e adicionar aqueles que estejam em falta. As dimensões são referenciadas nos factos e por isso devem ser carregadas primeiro. O exemplo que se segue demonstra um procedimento de \textit{ETL} criado no âmbito deste projeto para o carregamento de uma dimensão.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{etldimensao}
    \caption{Processo de \textit{ETL} para a dimensão da nacionalidade}
    \label{fig:etldimensao}
  \end{center}
\end{figure}

\paragraph{Facto} O método de carregamento de um facto difere do método usado para as dimensões. Como um facto é um registo de uma ocorrência da organizãção e pode ser eliminado, a forma mais correta para fazer o carregamento é apagar todos os dados que existam para o período temporal indicado e carregar de novo com a informação atualizada. O exemplo que se segue representa um processo de \textit{ETL} criado no âmbito deste projeto para um facto. Na imagem apresentada, é possivel reparar na  diferença entre o carregamento das dimensões e dos factos. A informação existente na tabela de facto, relativa ao periodo temporal que se pretende carregar (ano letivo), é apagada antes de efetuar o carregamento. Para além da diferença já apontada, a imagem ilustra as diferentes fases de um processo de \textit{ETL}: a extração dos dados, a transformação e o carregamento.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{etlfacto}
    \caption{Processo de \textit{ETL} para o carregamento do facto das vagas}
    \label{fig:etlfacto}
  \end{center}
\end{figure}

\subsubsection{\textit{Data Store} }

\textit{Data Store} representa o conjunto de todas as tabelas necessárias no \textit{DW}. As tabelas podem ser de dimensões ou factos e foram estruturadas segundo o \textit{star-schema}. Este esquema indica que a tabela de facto deve estar no centro da estrutura e relacionado através das suas chaves com as dimensões que referencia. 
Para além das tabelas de facto e dimensões, a solução incorpora um conjunto de tabelas, chamadas de tabelas de auditoria, para guardar a informação relativa às execuções dos pedidos de integração de informação e funcionamento da solução.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{starschemadw}
    \caption{\textit{Star-schema} para o facto das vagas}
    \label{fig:starschemadw}
  \end{center}
\end{figure}

\subsection{\textit{Data Presentation Area} }

A \textit{Data Presentation Area} tem como principal objetivo a manipulação da informação presente no \textit{DW} para disponibilizar a informação correta, devidamente estruturada, segundo o Modelo de Informação de Gestão. O resultado final desta camada resulta numa base de dados analítica para ser acedida pelos utilizadores finais, através das ferramentas de acesso aos dados.
A criação das diferentes camadas da \textit{Data Presentation Area} estão descritas nas secções seguintes. A ferramenta utilizada para criar a base de dados analítica foi o \textit{Visual Studio}. A camada de negócio é representada pelos \textit{Data Sources}, a camada de negócio está presente na \textit{Data Source View} e a camada de apresentação onde estão presentes as métricas ou também chamadas de indicadores e as suas dimensões estão presentes nas opções \textit{Cube} e \textit{Dimensions} respetivamente. As permissões de acesso aos dados podem ser configuradas na secção \textit{Roles}. 

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.3\textwidth]{vs2012}
    \caption{Estrutura da \textit{Data Presentation Area} no \textit{Visual Studio}}
    \label{fig:camadafisicassas}
  \end{center}
\end{figure}


\subsubsection{Camada Física} 
	A camada física é responsável pela ligação ao \textit{DW}, e assim permitir a integração entre a \textit{Data Store} e a \textit{Data Presentation Area}. A figura que se segue ilustra a camada física criada para a solução.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.55\textwidth]{camadafisicassas}
    \caption{Camada Física da \textit{Data Presentation Area}}
    \label{fig:camadafisicassas}
  \end{center}
\end{figure}


\subsubsection{Camada de Negócio}
	Depois de efetuada a ligação ao \textit{DW} na camada física, a camada de negócio é respnsável pela importação e mapeamento das tabelas necessárias para as dimensões e factos. Nesta camada podemos extender o modelo de dados importado da \textit{Data Store}. Este ponto torna-se bastante relevante pois, nesta fase, são tomadas decisões sobre a normalização dos dados que foram carregados da camada anterior. Para corresponder a requisitos de \textit{performance}, ou para garantir um tempo de desenvolvimento mais curto ou uma integração mais fácil com \textit{Data Marts} já existentes, é comum a criação de novas colunas ou tabelas originanado uma desnormalizção da estrutura que foi importada. As novas colunas têm a designação de \textit{Named Calculations} e as novas tabelas de \textit{Named Queries}. 

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{camadanegociossas}
    \caption{Camada de Negócio da \textit{Data Presentation Area}}
    \label{fig:camadanegociossas}
  \end{center}
\end{figure}


\subsubsection{Camada de Apresentação}

\paragraph{Dimensão}

Na camada de apresentãção, as dimensões são criadas com base nas tabelas de dimensão e nos atributos mapeados na camada de negócio. Cada atributo pode ser configurado segundo a sua visibilidade para o utilizador final, identificação ou formato de apresentação. Para além dos atributos, uma dimensão integra também o conjunto das hierarquias dos seus atributos. O exemplo mais comum de uma hierarquia é o calendário da dimensão temporal, neste caso, a hierarquia pode conter os atributos na seguinte ordem de grandeza: dia, semana, mês, trimestre e ano. As principais razões para se optar pela criação das hierarquias são: a facilidade de navegação (\textit{drill down and drill up} e a  criação automática índices e agregações pré-calculadas, agilizando o tempo de resposta das \textit{queries} requeridas pelos utilizadores finais.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{dimensaossas}
    \caption{Exemplo da dimensão temporal no \textit{Visual Studio}}
    \label{fig:dimensaossas}
  \end{center}
\end{figure}

\paragraph{Métrica / Indicador}

A criação dos indicadores é realizada ao nível do cubo multidimensional. Nesta fase, selecionamos os factos, passando a ser designados por \textit{measure groups} e a respestiva ligação com as dimensões que referenciam. Este conjunto de ligações entre as \textit{measure groups} e as suas dimensões origina uma matriz denominada de \textit{bus matrix}. A criação de uma \textit{bus matrix} torna-se complexa pois é necessário identificar corretamente todas as dimensões referenciadas pelos os \textit{measure groups}. 

Os seguintes tipos de ligações estão disponíveis durante a criação da \textit{bus matrix}:
\begin{itemize}
\item Regular: A relação regular entre uma dimensão e um \textit{measure group} existe quando a coluna chave para a dimensão está ligada diretamente à tabela de facto. Esta relação direta é baseada na relação de chave estrangeira com chave primária na \textit{Data Store}, mas também pode ser baseada numa relação lógica que é definida na camada de negócio. A relação regular, representa a relação entre tabelas de dimensão e uma tabela de facto no tradicional \textit{star-schema};
\item Referenciada: A relação referenciada entre uma dimensão e um \textit{measure group} existe quando a coluna chave para a dimensão é ligada indirectamente à tabela de facto por intermédio de uma chave numa outra tabela de dimensão;
\item Dimensão baseada no facto: Uma dimensão baseada num facto é geralmente designada por dimensão degenerada; estas dimensões são construídas através de atributos do facto. Por vezes, alguns atributos importantes são guardados nas tabelas de facto para evitar duplicação de informação;
\item \textit{Many-to-many}: No caso mais comum, cada \textit{measure group} liga a apenas um atributo de uma dimensão, contudo, um único registo de um facto pode ser, por vezes, associado a vários atributos de uma dimensão;
\end{itemize}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{busmatrixssas}
    \caption{Exemplo da \textit{bus matrix} no \textit{Visual Studio}}
    \label{fig:busmatrixssas}
  \end{center}
\end{figure}

Com a definição da \textit{measure groups} começa a fase de criação dos indicadores. Para a criação dos indicadores estão presentes dois tipos distintos. Quando a criação de um indicador baseia-se numa operação aritmética predefinida sobre um atributo do facto, o indicador criado é denominado de indicador base.
Para a criação dos indicadores base, a ferramenta tem a disposição um conjunto de operações predefinidas. O resultado da operação resulta num valor, num conjunto de valores ou num conjunto de registos o que origina tipos de agregação distintos da informação. O conhecimento do tipo de agregação da informação resultante é importante quando os indicadores criados servirão de base para o cálculo de outros indicadores no decorrer da implementação da solução.
Entre todas as operações predefindas existentes, destacam-se as mais utilizadas para os tipos de indicadores mais comuns:

\begin{itemize}
	\item Soma: a soma de todas as ocorrências;
	\item \textit{Count of rows}: número de ocorrências;
	\item \textit{Count of non-empty values}: número de ocorrências cujo o valor é diferente de \textit{null};
	\item Mínimo: valor mínimo do conjunto de ocorrências;
	\item Máximo: valor máximo do conjunto de ocorrências;
	\item \textit{Distinct count} : número de ocorrências distintas.	
\end{itemize}

Um indicador calculado existe quando não estão presentes as operações aritméticas predefinidas necessárias para obter o seu valor. Estes indicadores são definidos com base numa fórmula de cálculo, presente no Modelo de Informação de Gestão, que deve ser implementada recorrendo a uma linguagem \textit{script} própria. Para a criação deste indicadores, a ferramenta interpreta a sintaxe \textit{MDX}.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{mdxsintax}
    \caption{Sintaxe \textit{MDX} para a criação de indicadores}
    \label{fig:mdxsintaxz}
  \end{center}
\end{figure}

Cada indicador fica associado a um \textit{measure group} relacionado com a tabela de facto que o originou e com o tipo de agregação. O conjunto de indicadores, agrupados nos seus grupos e relacionados com as dimensões na \textit{bus-matrix} originam a criação de um cubo multidimensional \textit{OLAP}. 

\paragraph{Base de dados analítica}

Para aceder e consultar os cubos muldimensionais criados, estes são suportados por uma base de dados analítica criada com o \textit{Microsoft Analysis Service}. Desta forma é possível ter um conjunto de cubos muldimensionais disponíveis para serem acedidos e consultados. Para isso, apenas é necessário processar o cubo e disponibilizar a base de dados que os contém sobre o serviço já especificado. O processamento do cubo representa o carregamento da informação presente na \textit{Data Store} para o cubo e a criação das restantes componentes, ficando associado a uma única base de dados analítica. Para concluir, é importante referir que, apenas durante a fase de processamento do cubo, a informação fica disponivel para o utilizador final. Com isto, podemos concluir que, qualquer alteração da informação nos sitemas fonte apenas é visível depois do seu carregamento na \textit{Data Store} e posterior processamento do cubo do \textit{Data Mart} correspondente.

\subsubsection{Ferramentas de acesso aos dados}

A última etapa desta fase de implementação tinha como principal objetivo a disponibilização da informação nas duas plataformas encontradas que preenchiam o conjunto de requisitos da solução. Apesar das diferenças já referidas nos capítulos anteriores, é importante que, qualquer que sejam as plataformas escolhidas, respondam a uma conjunto de necessidades comuns das solução de \textit{BI}. Estas plataformas devem ter a possibilidade de definir permissões a nível individual ou departamental (grupos), devem permitir a exportação dos \textit{dashboards} e \textit{reports} e a colaboração e corporação dos diferentes membros dentro das organizações.

\paragraph{\textit{Sharepoint}}

A plataforma colaborativa \textit{Sharepoint} revelou-se uma mais valia para a solução final, principalmente, devido à possibilidade de montar toda a arquitetura da solução \textit{on premise}, a possibilidade de usar \textit{Excel Services} para obter um nível detalhado de análise de cada \textit{report} e os seus custos reducidos na implementação e integração por ser a solução usada até ao momento. Contudo, a incompatibilidade com alguns \textit{browsers} e dispositivos móveis bem como os elevados custos de infrastrutura necessários revelaram-se as desvantagens desta opção.

\paragraph{\textit{Power BI}} 

A plataforma \textit{Power BI} surgiu com o objetivo de resolver as desvantagens encontradas com a solução anteriormente apresentada e cumpriu os objetivos que tinha sido apresentados. O \textit{Power BI} é uma solução baseada na \textit{cloud}, compatível com os vários dispositivos móveis e \textit{browsers}, contudo, devido a esta solução ter estado em desenvolvimento no decorrer da implementação desta solução, foi necessário dispender algum tempo para perceber as alterações e funcionalidades que surgiam com cada nova \textit{release} da solução. Até ao momento, a grande desvantagem encontrada com esta opção prende-se com a impossibilidade de utilizar as análises detalhadas criadas e disponibilizadas em \textit{Excel}.

\section{Formação / Documentação}

Como o desenvolvimento da solução tinha como utilizador final a Indra, na etapa de fomração e documentação apenas foi necessário produzir a documentação orientada aos desenvolvimentos que foram feitos durante este projeto. Para além do Manual de Informação de Gestão que já foi referido, a solução deve fazer-se acompanhar do Documento Técnico que inclui, para além das configurações técnicas necessárias para utilizar a solução, um manual de utilizador com os corretos procedimentos para o bom funcionamento dos processos de integração da informação e utilização das plataformas de exploração da informação. A informação mais relevante destes documentos encontra-se devidamente anexada a este relarório. \ref{anexoc}


\section{Testes de Aceitação}

Os testes de aceitação da solução dividiram-se em 4 etapas. 

A primeira etapa tinha como principal objetivo validar a integração dos sistemas fonte e dos processos de \textit{ETL}. 

A etapa seguinte prendia-se com a análise da \textit{Data Store}. Nesta etapa, a estrutura é validada, bem como os diferentes tipos de dados utilizados, a utilização de chaves primárias e estrangeiras e o conjunto dos dados resultante do carregamento da informação. 

A fase seguinte valida os resultados presentes no cubo multidimensional criado na base de dados analítica. O conjunto de indicadores e dimensões que constituem o cubo são comparados e validados segundo o Modelo de Informãção de Gestão desenvolvido na fase de levantamento de requisitos.

A última etapa de valiadação e aceitação passa pelas plataformas aplicacionais. Nesta fase, o conjunto de \textit{dashboards} e \textit{reports} são validados e são aceites, caso cumpram os requisitos definidos pelo utilizador final.
A figura que se segue demonstra um conjunto de requisitos das plataformas aplicacionais, presente no documento que serve de apoio durante este fase de testes. Este documento é esperado como \textit{deliverable} desta fase. 

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{testes}
    \caption{\textit{Template} dos testes de aceitação realizados às plataformas aplicacionais}
    \label{fig:testes}
  \end{center}
\end{figure}



\section{Entrada em Produção}


A entrada em produçao da solução foi a última etapa no desenvolvimento deste projeto. Nesta etapa era necessário incorporar a solução no produto existente, \textit{iBISmartGIAF}, sendo o resultado final o \textit{deliverable} desta última etapa. 
A ilustração que se segue representa a arquitetura final da solução.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{arquiteturafinal}
    \caption{Arquitetura final da solução}
    \label{fig:arquiteturafinal}
  \end{center}
\end{figure}
