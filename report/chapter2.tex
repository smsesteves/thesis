\chapter{Enquadramento Teórico} \label{chap:2}

A par das pessoas, a informação é considerada o ativo mais importante para qualquer organização. 

Facilmente encontramos diversas definições para o conceito de informação. Por exemplo, consultando o dicionário \textit{Merriam-Webster} ~\cite{kn:ISMerriam}, encontramos a  seguinte definição: \textit{É a resposta a uma pergunta de algum tipo. É também a partir do qual os dados e o conhecimento podem derivar, os dados representam valores atribuídos aos parâmetros e conhecimento significa compreensão das coisas reais ou conceitos abstratos}. 

Assim, é possível inferir que a informação está diretamente relacionada com os dados e com o conhecimento que podemos retirar deles.


Em 1998, Devenport, definiu uma correlação entre estes três conceitos: dados, informação e conhecimento ~\cite{kn:ISDevenport}.

O dado resulta de simples observações sobre o estado atual do mundo, pode ser obtido por máquinas e facilmente quantificado. 
A informação é um conjunto de dados dotados de relevância e propósito. Requer uma unidade de análise, exige necessariamente a medição humana e consenso em relação ao seu significado.
Por fim, o conhecimento é a informação valiosa. Inclui reflexão, síntese e contexto. O conhecimento dificimente consegue-se obter por máquinas, é difícil a sua estruturação e é frequentemente tácito, isto é, algo implícito ou subentendido, sendo dispensável explicações ou menções a respeito.


Os dados também podem ser um conjunto de informações que constituem a informação.
\textit{Já o conhecimento, refere-se à habilidade de criar um modelo que descreva o objeto e indique as ações a implementar e as decisões a tomar} ~\cite{kn:ISRezende} .

\section{Sistemas de Informação}


Segundo a definição presente no estudo publicado sobre sistemas de informação com o título \textit{Interdisciplinary Aspects of Information Systems Studies} ~\cite{kn:ISBook}, um Sistema de Informação é um sistema composto pelas pessoas e computadores que processam e interpretam informação. 

O glossário do \textit{Software Enginnering Institute} da Universidade Carnegie Mellon de 2007, descreve um sistema de informação como um sistema que aglomera qualquer tecnologia com as atividades realizadas pelas pessoas que usam a primeira de forma a apoiar, recolhendo, filtrando, criando e distribuindo dados.

Num livro publicado em 2008 de Jessup e Valacich ~\cite{kn:ISJessup} também encontramos esta proposta de definição.

Em 1992, também Alter já definiu um Sistema de Informação como um sistema que aglomera a tecnologia e as atividades realizadas, contudo, a sua definição era um pouco mais abrangente: um sistema de informação é uma combinação de trabalho, informações, pessoas e tecnologias de informação organizados para alcançar os objetivos de uma organização ~\cite{kn:ISAlter}.

Mais recentemente, no ano de 2013, Simon Bulgacs, identificou um sistema de informação como um qualquer sistema que apoia as operações, a gestão e a tomada de decisão ~\cite{kn:ISBulgacs}. Esta definição surgiu com base na descrição feita por Kroenke, em 2008, que afirmava que um sistema de informação era uma tecnologia de informação e comunicação que uma organização usava e ainda a forma como as pessoas interagiam com a tecnologia que surportava o processo de negócio ~\cite{kn:ISKroenke}.

A estrutura clássica dos sistemas de informação encontrada em livros que datam de 1988 ~\cite{kn:ISLaudon} estaria representada sob a forma de uma pirâmide refletindo a hierarquia da organização. Geralmente, incluía os sistemas de processamento de transações no fundo da pirâmide, seguidos pelos sistemas de informação para a gestão, suporte à decisão e terminando com os sistemas de informação executivos no topo da pirâmide como demonstra a figura seguinte.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.65\textwidth]{Four-Level-Pyramid-model}
    \caption{Modelo de pirâmide dos Sistemas de Informação }
    \label{fig:arquitecture}
  \end{center}
\end{figure}

Partindo da estrutura apresentada, podemos reparar que existem diferentes  sistemas de informação dedicados a certas funções ou áreas dentro das organizações.


Os sistemas de processamento de transações (\textit{SPT}) são sistemas que processam as informações detalhadamente e que são necessárias para a atualização dos dados sobre as operações de uma organização.

Os sistemas de apoio à gestão (\textit{MIS}) tem como objetivo a gestão eficaz de qualquer organização, estabelecendo desde logo uma diferença para os restantes devido ao seu foco na análise da informação estratégica e das atividades operacionais. 

Os sistemas de apoio à decisão (\textit{DSS}) têm como principal objetivo auxiliar a tomada de decisões sobre o negócio ou sobre a organização. O \textit{DSS} serve o nível da gestão, operação e planeamento da organização.

Estes dois últimos sistemas de informação, \textit{MIS} e \textit{DSS}, são sistemas dedicados à gestão eficiente de cada organização mas diferenciam-se dos restantes sistemas de informação porque focam a sua atividade na análise de informação estratégica das ativdades operacionais. Os sistemas de apoio à gestão (\textit{MIS}) apareceram primeiro e, segundo O'Brien, os primeiros \textit{MIS} que surgiram eram focados em dados sobre vendas, inventários e compras ~\cite{kn:OBrien}. Quando surgiram os sistemas de apoio à decisão, DSS, algumas foram as definições e implementações que colocaram estes dois sistemas no mesmo nível da pirâmide, contudo, apesar de reconhecerem as várias semelhanças não deixaram de apontar também como grande fator de diferenciação destes novos sistemas o foco em problemas menos especificados e estruturados que os DSS conseguiram então responder ~\cite{kn:Sprague}.


Os sistemas de informação executivos (\textit{EES}) são reconhecidos como um tipo de \textit{software} de apoio à gestão (MIS) que suporta a tomada de decisão (\textit{DSS}) no nível mais alto da organização. Este sistemas devem providenciar acesso à informação interna e externa relevante para a organização atingir os seus objetivos.

Embora o modelo apresentado continue a ser útil, desde que surgiu pela primeira vez um conjunto vasto de novas tecnologias, desenvolveram-se novas categorias de sistemas de informação, alguns dos quais já não se encaixam facilmente no modelo apresentado. O exemplo mais comum são os sistemas de \textit{ERP}.

Na figura que se segue são destacadas as diversas áreas onde os sistemas \textit{ERP} são utilizados. Estes sistemas são classificados como \textit{Business Management Software}.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{ERP_Modules}
    \caption{Modelo \textit{ERP} }
    \label{fig:erpmodules}
  \end{center}
\end{figure}


\section{Sistemas de \textit{Business Intelligence} }

O conceito \textit{Business Intelligence} foi utilizado pela primeira vez, em 1865, por Richard Millar Devens no seu livro \textit{Cyclopædia of Commercial and Business Anecdotes}. 
Devens usou este termo para descrever como foi possível o banqueiro, \textit{Sir} Henry Furnace, ganhar lucros ao receber e agir de acordo com informações sobre o seu negócio, antes mesmo dos seus concorrentes. ~\cite{kn:ISDevens}

Mais tarde, em 1958, um investigador da empresa IBM, Hans Peter Luhn, definiu \textit{Business Intelligence} como a capacidade de apreender as inter relações dos factos apresentados de forma a orientar a ação para um determinado objetivo. ~\cite{kn:ISHans}

Embora existam outras definições, todas apontam para definir os sistemas de \textit{BI} como sistemas com a principal tarefa de apoiar a tomada de decisão por parte da gestão de topo.

O termo \textit{BI} foi popularizada por Howard Dresner, em 1989, descrevendo-o como \textit{ um conjunto de conceitos e métodos para melhorar a tomada de decisão do negócio através da utilização de sistemas de suporte à decisão} ~\cite{kn:Dresner}.

Thomsen ~\cite{kn:Thomsen}, em 2003, referiu ainda que estes sistemas vieram substituir os DSS, MIS e EIS pois estes sistemas não eram capazes de satisfazer um conjunto de necessidades que se revelaram os principais objetivos dos sistemas de \textit{BI}. Um ano mais tarde, Negash, definiu os sistemas de \textit{Business Intelligence} como a combinação da recolha e armazenamento de dados operacionais com as ferramentas de análise de forma a apresentar informação competitiva e complexa aos decisores ~\cite{kn:ISNegash}.

Os principais objetivos de um sistemas de \textit{Business Intelligence} ~\cite{kn:BIObjs} são:

\begin{itemize}
  \item Melhorar o desempenho da organização;
  \item Evitar perdas financeiras;
  \item Identificar oportunidades futuras;
  \item Motivar os colaboradores;
  \item Explicitar expectativas de desempenho;
  \item Melhorar a comunicação;
  \item Antecipação de problemas futuros.
\end{itemize}

Em 1992, Sprague e Carlson ~\cite{kn:SPCarlson} já tinham identificado este conjunto de necessidades, definindo o processo para a tomada de decisão em quatro fases: inteligência, desenvolvimento e análise de situações alternativas, escolha e implementação. Enquanto que os \textit{MIS} atuavam apenas na primeira fase, inteligência, todas as outras 3 fases eram cobertas pelos \textit{DSS}, contudo, tanto os \textit{DSS} como os \textit{MIS} não permitiam um controlo e utilização com um uso definido pelo utilizador final e ainda tinha uma outra limitação: impossibilidade de cruzar dados provenienetes de outros sistemas de informação.


\subsection{Conceito de Data Warehouse}

O termo \textit{Data Warehouse} é conhecido como um Armazém de Dados e pode ser visto como uma base de dados utilizada para consultas, análise de dados, modelação do negócio e \textit{reporting}. Em 2009, Turban ~\cite{Kn:Turban}, definiu DW como um sistema de \textit{BI} baseado num banco de dados orientado para consultas complexas, permintindo reduzir os acessos aos sistemas operacionais e obter uma visão geral do negócio da organização.

Os principais objetivos de um \textit{DW} são ~\cite{Kn:KimballRoss}:

\begin{itemize}
  \item Facilitar o acesso à informação da organização;
  \item Apresentar dados consistentes;
  \item Manter a segurança dos dados;
  \item Deverá ser esta a estrutura que surportará a tomada de decisão;
  \item Deve ser aceite por todos os elementos da organização como sendo uma representação fidedigna das áreas de negócio a analisar.
\end{itemize}



\subsubsection{Metodologia Inmon}

A metodologia proposta por William Bill Inmon, conhecido como "Pai do DW", aposta na criação de um \textit{DW} que abranga toda as áreas de uma organização. Inmon definiu o \textit{DW} como \textit{a collection of integrated,subject-oriented, time variant and nonvalatile databases designed to support DSS (decision support) function, where each unit of data is non-volatile and relevant to some moment in time} ~\cite{Kn:InmonDW}. Esta proposta de Inmon indica que, partindo de um repositório central normalizado, devem ser adicionados \textit{Data Marts} de forma a colmatar as necessidades de cada departamento da organização. Um \textit{Data Mart} é um parte da totalidade das organizações, sendo que apenas apresenta os dados de um único processo de negócio ~\cite{Kn:ISPaulraj}.
A metodologia proposta por Bill Inmon também é conhecida como uma abordagem \textit{top-down} .


\subsubsection{Metodologia Kimball}

Ralph Kimball, conhecido como "Pai do \textit{BI}", aponta uma metodologia para a criação de um \textit{DW} baseada na criação de \textit{Data Marts} com o objetivo de atingir uma análise e \textit{reporting} departamental. Partindo do \textit{DW} criado, Kimball propõe que os diferentes \textit{DM} modelados sejam virtualmente integrados usando dimensões. Esta abordagem de Ralph Kimball é conhecida como \textit{bottom-up} .


\subsubsection{Metodologia Inmon vs Metodologia Kimball}

Associadas a estas duas metodologias abordadas, existem diferentes formas de implementação, bem como características, vantagens e desvantagens. As tabelas que se seguem resumem cada um destes aspetos comparando as metodologias propostas por Inmon e Kimball.

\begin{table}[H]
  \centering
  \caption{Metodologia Inmon vs Metodologia Kimball. Fonte: ~\cite{Kn:George} }
\begin{tabular}{ | l | c | c | }
  \hline
  \textbf{} & \textbf{Inmon} & \textbf{Kimball} \\
  \hline
  \hline
        Ferramentas Dimensionais & & \checkmark \\ \hline
        Orientada aos processos & & \checkmark \\ \hline
        Facilidade de \textit{design} & & \checkmark \\ \hline
        Ferramentas relacionais & \checkmark & \\ \hline
        Dados normalizados & \checkmark & \\ \hline
        Análise temporal contínua e discreta & \checkmark & \\ 
  \hline
\end{tabular}
  \label{tab:inmonkimball}
\end{table}



\begin{table}[H]
  \centering
  \caption{Vantagens e Desvantagens das metodologias. Fonte: ~\cite{Kn:George} }
\begin{tabular}{ | l | c | c | }
  \hline
  \textbf{} & \textbf{Inmon} & \textbf{Kimball} \\
  \hline
  \hline
        Construção do \textit{DW} & Dispendiosa & Simples \\ \hline
        Manutenção & Fácil & Difícil e redundante \\\hline
        Custo & Inicialmente alto, com custos decrescentes & Baixo, com custos idênticos \\ \hline
        Duração & Longo tempo de implementação & Curto tempo de implementação \\ \hline
        Conhecimento & Equipa especialista & Equipa generalista \\ 
  \hline
\end{tabular}
  \label{tab:inmonvskimball}
\end{table}




A metolodgia seguida neste projeto foi baseada na metodologia proposta por Kimball, à qual os próximos pontos estão orientados.


\subsubsection{Modelação entidade-relação}

Podemos reparar na tabela anterior que Kimball aponta para ferramentas dimensionais contrastando com as ferramentas relacionais propostas por Inmon.

A modelação entidade-relação, defendida por Inmon, descreve o \textit{DW} como uma base de dados que usa técnicas de modelação entidade-relação ~\cite{Kn:Sabhewal} .

Esta técnica exige a criação de um repositório central de dados com o objetivo de armazenar a informação corporativa de negócio. Este repositório é designado de \textit{Enterprise Data Warehouse (EDW) }. São usados processos de extração, transformação e carregamento da informação, \textit{ETL}, para integrar e consolidar os dados, remover as incoerências, antes de serem armazenados no \textit{DW}. Os dados provenientes dos sistemas de informação operacionais são armazenadas na 3ª forma normal com o objetivo de evitar a sua redundância ~\cite{Kn:Boehnlein}.

O \textit{EDW} é a fonte de informação para os \textit{Data Marts}. Uma vez implementado o repositório de dados, são criados os diferentes \textit{Data Marts} que contém os dados na forma não normalizada. A principal razão pela qual encontramos os dados não normalizados nos \textit{Data Marts} prende-se com a necessidade de fornecer um acesso mais rápido aos dados para os utilizadores finais.


A figura que se segue demonstra a arquitetura do DW segundo a proposta de Inmon.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{InmonArq}
    \caption{Arquitetura do \textit{DW} proposta por Inmon Fonte:~\cite{kn:Nuno}}
    \label{fig:arquitectureinmon}
  \end{center}
\end{figure}



As principais vantagens desta abordagem são: a criação de um repositório uníco centralizado para fornecer uma visão dos dados empresariais fiável e ainda fornece uma grande flexibilidade para a criação dos \textit{Data Marts}.
Contudo, o investimento inicial para a criação da solução pode ser a grande desvatagem encontrada para seguir esta metodologia proposta por Inmon.


\subsubsection{Modelação dimensional}

A modelação sugerida por Kimball assenta a sua estruturação em dois conceitos que devem ser mencionados: facto e dimensão.

Facto, por vezes referido como medida, métrica ou indicador, é geralmente conhecido como uma ocorrência do negócio que pode ser mensurável. Dimensão é um conjunto de hierarquias e atributos que definem um facto, ou seja, descreve uma mesma propriedade da ocorrência, apresentando um conjunto de chaves, atributos e valores que podem ser combinados entre os vários \textit{Data Marts} . A título de exemplo, facto pode ser o número de inscrições e ter como dimensões de análise a escola, curso, entre outras.

Como já foi visto anteriormente, Kimball propõe a criação de vários \textit{Data Marts} que podem ser construídos em períodos distintos, em fases distintas da implementação da solução. Antes da construção do \textit{DM} devem ser reunidas todas as necessidades do departamento e identificar as dimensões que possam ser partilhadas entre departamentos. O caso mais comum é a dimensão temporal, esta dimensão está presente em praticamente todas as análises necessárias. Este tipo de dimensão é denominada de conforme ~\cite{kn:Kimbal2012}.

A forma mais comum e simples de construir um \textit{DM} é seguir o chamado \textit{star-schema}. Este esquema é composto pela tabela de facto no centro, referenciando todas as suas dimensões conformes ~\cite{kn:CJDate}.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.4\textwidth]{starschema}
    \caption{\textit{Star Schema} }
    \label{fig:starschema}
  \end{center}
\end{figure}



\subsection{Arquitetura de um Data Warehouse}



Como referido, a metodologia proposta por Kimball será a metodologia seguida durante este projeto, por isso, durante esta secção todos os componentes presentes na arquitetura do \textit{DW}, proposta por Kimball, serão devidamente especificados e descritos.

Esta arquitetura encontra-se representada na figura que se segue.


\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{KimballArq}
    \caption{Arquitetura do \textit{DW} proposta por Kimball Fonte: ~\cite{kn:KimballToolkit}}
    \label{fig:arquitecturekimball}
  \end{center}
\end{figure}


\subsubsection{Sistemas Fonte Operacionais}

Os Sistemas Fonte são referenciados como a fonte de informação para as soluções de \textit{BI}. 
Estas soluções podem combinar diferentes sistemas, com estruturas distintas, com o objetivo de responder às necessidades da solução. Estes sistemas fonte operacionais geralmente são designados por \textit{OLTP} e têm como principal função capturar as transações de negócio da organização em tempo real. Devido a esta restrição, as consultas de informação por parte dos sistemas de \textit{Business Intelligence} apresentam a obrigatoriedade de serem guardadas no sistema, e asssim, a solução ser capaz de fornecer informação dos dados de histórico referente à organização. Este fator é relevante porque a tomada de decisão envolve comparações temporais ~\cite{kn:BallouTayi}. 

A informação fornecida é passada para a \textit{Data Staging Area}, em conjunto com a restante informção adicional necessária, composta geralmente por ficheiros de texto, \textit{Excel}, entre outros.



\subsubsection{\textit{Data Staging Area} }


\paragraph{\textit{Processing}}

A extração é o primeiro passo para  recolher e inserir os dados necessários no \textit{DW}. Para a extração ser bem sucessida é necessário perceber a fonte de dados, interpretar a sua estrutura e identificar as neccessidades. Depois da extração, a etapa seguinte é a etapa de transformação. Neste etapa, os dados são limpos, descartados os dados desnecessários e repetidos, adaptados aos formatos pretendidos e ainda tratados aqueles que dizem respeito a situações que não estão em conformidade com o esperado.
Por fim, a etapa final é o carregamento, nesta fase, os dados resultantes da fase de extração e transformação são carregados nas tabelas correspondentes no \textit{DW} . A estrutura do \textit{DW} deve estar já orientada para o modelo dimensional.

Os processos que pertencem a estas etapas são denominados de \textit{extract-transformation-load}, \textit{ETL} ~\cite{kn:KimballRoss2002}.

Segundo Ponniah ~\cite{kn:Ponniah2001}, a implementação dos processos de \textit{ETL} ocupa cerca de 50 a 70 por cento do tempo total previsto de implementação dos projetos de \textit{Business Intelligence}.

\paragraph{\textit{Data Store} }

\textit{Data Store} representa o conjunto de todas as tabelas necessárias no \textit{DW} para permitir análises complexas com a construção dos \textit{DM} . A estrutura destas tabelas contrasta com a estrutura dos sistemas operacionais fonte pois, estes estão altamente normalizados para que seja possível efetuar \textit{queries} com a maior rapidez possível.
As atualizações do \textit{DW} através dos processos de \textit{ETL} podem ser configuradas para extrair, transformar e carregar os dados para o \textit{DW} diariamente, mensalmente, anualmente, ou, se assim se justificar, até mesmo em tempo real.


\subsubsection{\textit{Data Presentation Area} }

Este componente da arquitetura de um \textit{DW} também por vezes é conhecido como \textit{Metadata}. Na \textit{Data Presentation Area} é possível manipular a informação presente no \textit{DW} para disponibilizar a informação correta e devidamente estruturada para ser acedida pelos utilizador finais.

A \textit{Medata} está organizada em 3 camadas:
\begin{itemize}
  \item Física: Nesta camada é definida a ligação ao \textit{DW} e selecionadas as tabelas necessárias;
  \item Negócio: Na camada de negócio são mapeadas as tabelas importadas do \textit{DW} que servirão de base para as dimensões e factos;
  \item Apresentação: Na camada de apresentação são disponibilizados os indicadores resultantes dos factos e as dimensões de análise bem como as suas hierarquias. O conjunto dos indicadores e dimensões é disponibilizado para ser acedido pelas ferramentas de \textit{reporting}.
\end{itemize}


\subsubsection{Ferramentas de acesso aos dados}


Um ferramenta de acesso aos dados pode ser uma ferramenta de consulta \textit{ad hoc} ou com mais complexidade, uma ferramenta de \textit{Data Mining} ou \textit{Forecasting} ~\cite{Kn:NestorovJukic} .
A forma mais comum dos utilizadores finais acederem ao dados é através de aplicações analíticas para exploração da informação.
As plataformas de exploração e acesso da informação têm como principal objetivo criar, editar e disponibilizar as análises e relatórios desenvolvidos, possibilitando a sua partilha dentro da organização. 


\section{Âmbito da solução}

\subsection{Gestão Académica}
A solução apresentada neste documento tem como principal objetivo melhorar a eficácia na gestão das instituições de Ensino Superior. Assim sendo, foi necessário estudar as áreas de interesse para a gestão com a intenção de incorporarem a solução final. 

A análise efetuada revelou a necessidade de incorporar na solução as seguintes áreas:

\begin{itemize}
  \item Procura 
  \item Eficácia
  \item Receitas
  \item Custos
\end{itemize}

A Procura de uma determinada escola ou curso será analisada tendo como base os dados do Concurso Nacional de Acessos, as vagas disponíveis e ainda as inscrições feitas no \textit{software} de apoio à Gestão Académica das Instituições de Ensino Superior. 

A Eficácia de uma determinada unidade curricular pode ser analisada usando os dados disponíveis no \textit{software} de apoio à Gestão Académica, analisando, por exemplo, a taxa de assiduidade, percentagem de aprovações ou ainda o número de inscrições. 
Este conceito da eficácia pode ser analisado num universo mais abrangente como são os cursos ou as escolas, mas, sendo assim, devem ser analisados os dados relativos aos diplomados, classificações finais de curso ou ainda o número de inscrições para a conclusão do mesmo.

Os indicadores associadados à procura e eficácia de uma escola ou curso superior podem ainda ser medidos num universo paralelo ao \textit{software} de apoio à Gestão Académica. 
Durante a fase de levantamento de requisitos, uma das necessidades que foi encontrada prendia-se com o facto de analisar estes indicadores de acordo com os inquéritos RAIDES (Registo de Alunos Inscritos e Diplomados do Ensino Superior) ~\cite{kn:Raides} submetidos pelas unidades de Ensino Superior. Estes inquéritos são submetidos ao Ministério da Educação e servem de base ao orçamento disponibilizado a cada Institução. Por isso, podemos concluir que se torna imperativo a sua implementação para disponibilizar esta informação tão relevante à gestão das unidades de ensino.

As Receitas e Custos terão como base o \textit{ERP} da Indra e ainda o \textit{software} de apoio à Gestão Académica. O objetivo dos indicadores presentes nestes dois conceitos é tentar perceber até que ponto é possível inferir um custo de uma unidade curricular ou curso, através do somatório de todos os custos e encargos dos docentes desses mesmos cursos ou unidades curriculares. De salientar que apenas serão considerados custos diretos para esta análise.
Os valores da receita que cada escola recebe ao longo do ano provenientes das inscrições dos estudantes terão também relevância e serão alvo de uma análise com um conjunto de indicadores apropriados. Este tema será subdividido em receita gerada e receita cobrada.

Por fim, um dos problemas encontrados, para além dos problemas já descritos, é a forma como estes dados podem ser explorados. 
Hoje em dia, as plataformas de exploração de informação têm evoluído a um ritmo elevado e, neste momento, analisando a oferta existente, torna-se essencial inferir a que melhor se adapta à organização.
Este último tópico será alvo de uma análise crítica com o objetivo de propor alternativas à plataforma proposta pela Indra.


\subsection{Projetos Existentes}

Durante a análise deste projeto, foi desenvolvida uma pesquisa para encontrar soluções semelhantes realizadas no mesmo âmbito. Os sistemas de informação, que são usados para a Gestão Académica hoje em dia, têm a possibilidade de exportar relatórios e algumas análises gráficas, contudo, este tipo de soluções não responde a todas as necessidades encontradas para esta área. As soluções existentes não são capaz de trazer a capacidade de personalização e adaptação às necessidades do utilizador final, os relatórios e análises produzidos por estes sistema estão circunscritos à implementação rígida e parametrizada que foi definida. 

As soluções de \textit{Business Intelligence} tendem a penetrar cada vez mais dentro das organizações e, assim sendo, foram encontradas algumas implementações em áreas mais comuns de negócio como Recursos Humanos, Logística ou Financeira.
Alguns dos exemplos encontrados estão presentes nos seguintes relatórios ~\cite{kn:Arqu} , ~\cite{kn:SecondCenas}, ~\cite{kn:Nuno} ~\cite{kn:Duarte}. 

Devido aos resultados desta pesquisa, torna-se relevante responder às necessidades das Instituições de Ensino Superior, através da implementação de uma solução de \textit{BI}, para serem mais um exemplo de sucesso no uso destes novos sistemas de informação, beneficiando das mais valias que estão inerentes à sua utilização.

\subsection{Solução Atual - iBISmartGiaf}

A solução de \textit{BI} oferecida pela Indra no âmbito das Instituições de Ensino Superior, antes da execução deste projeto, contemplava as áreas de Recursos Humanos e Financeira.
Baseia-se num sistema aberto à evolução, podendo integrar facilmente novas áreas ou melhoramentos das existentes. É uma solução modular e departamental, metodologia proposta por Kimball, baseada em \textit{Data Marts} permitindo uma implementação faseada e rápida. A solução pode ser disponibilizada usando tecnologias \textit{Oracle} ou \textit{Microsoft} . 

A figura que se segue demonstra a arquitetura da solução existente.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{Arquiteturaold}
    \caption{Arquitetura da solução atual - \textit{iBISmartGIAF}} 
    \label{fig:arquiteturaold}
  \end{center}
\end{figure}


Para personalizar e configurar a solução, existe ainda um portal denominado de \textit{Back Office} onde é possível agendar os processos de integração, analisar o seu resultado e configurar regras de negócio específicas para cada área de intervenção.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{backoffice_1}
    \caption{Portal de \textit{Back Office} - \textit{iBISmartGIAF} }
    \label{fig:backofficeold}
  \end{center}
\end{figure}


A solução que resultará deste projeto deve ser totalmente compatível com o sistema atual e de fácil integração.

\subsection{Tecnologias}

As tecnologias presentes nos dias de hoje para o desenvolvimento de soluções de \textit{BI} tendem a modificar imenso devido à evolução tecnológica constante que a área tem sofrido. Contudo, podemos desde já enunciar algumas tecnologias existentes que dão resposta às necessidades do mercado atual. 

A Microsoft, não tendo uma \textit{suite} de \textit{BI} incorporando todas as ferramentas necessárias, contém mum conjunto de serviços que em conjunto possibilitam a criação de uma solução. Estas ferramentas são: \textit{Microsft SQL Server}, \textit{Micrososft Integration Services}, \textit{Micrososft Analysis Services} e \textit{Micrososft Reporting Services} ~\cite{kn:bisite}. Na \textit{cloud}, a \textit{Micrososft} tem à disposição dos utilizadores a plataforma \textit{Power BI} ~\cite{kn:powerbi} que tem como principal argumento no mercado ser uma plataforma colaborativa, baseada em tecnologias \textit{Web}, que permite a exploração da informação orientada as novas realidades do mercado como a compatibilidade com dispositivos móveis ou a \textit{cloud}. De salientar que esta ferramenta apenas permite a exploração da informação e o \textit{reporting} , a construção dos \textit{DM} tem de ser feita recorrendo às restantes ferramentas da \textit{Microsoft}

A Oracle disponibiliza uma ferramenta que inclui todas as funcionalidades para permitir a criação destas soluções. Esta solução tem o nome de \textit{Oracle Business Intelligence Enterprise Edition} e encontra-se na versão 12g atualmente ~\cite{kn:obiee}. 

Apesar destas duas soluções serem as opções mais utilizadas no mercado atual, também é relevante citar a solução denominada de \textit{Pentaho} ~\cite{kn:pentaho}, solução desenvolvida sobre um projeto \textit{open source} e ainda outras duas: \textit{Microstrategy} ~\cite{kn:microsotrategy} e \textit{Qlik} ~\cite{kn:qlik}.

Existem muitas outras alternativas no mercado e a sua escolha terá sempre de ser adaptada à organização que utilizará a solução de \textit{BI}.
Hoje em dia, a escolha entre cada ferramenta tecnológica é feita com base nos requisitos e na organização onde a solução estará inserida, assim sendo, na fase de escolha da plataforma tecnológica são tidos em conta aspetos como: a possibilidade de usar \textit{cloud}, possibilidade de personalização, integração nos sistemas já existentes, entre outros.

Para este projeto, um dos requisitos da solução seria a fácil incorporação da mesma na solução existente, assim sendo, à partida apenas a \textit{suite} da \textit{Oracle} ou as tecnologias da \textit{Microsoft} poderiam ser soluções para os desafios tecnológicos da implementação deste projeto. 
Analisando as duas opções possíveis, facilmente concluímos que não existe nenhuma vantagem, no decorrer da implementação e futura utilização da solução, em escolher uma das opção em detrimento da outra. Assim sendo, a escolha prende-se mais pela sua fácil adaptatibilidade dentro da organização a que se destina e não nos desafios ou facilidades que pode trazer no decorrer da fase de implementação.
Concluindo, analisando as organizações que são o público alvo da solução, conseguimos perceber que os custos de desenvolvimento de uma solução de \textit{BI} dentro das Instituições de Ensino Superior acaba por tornar a opção da \textit{Microsoft} mais vatanjosa devido ao número de elevado de parcerias que se encontram estabelecidas. 
A necessidade de analisar esta vertente de negócio prende-se com os elevados custos que estas soluções podem ter de suportar com o seu licenciamento e, neste caso, os custos inerentes a uma solução \textit{Micrososft} revelam-se mais vantajosos. 
Com a escolha definida sobre o conjunto de ferramentas da \textit{Microsoft}, é necesário identificar as diferentes plataformas de exloração da informação existentes, compatíveis com a escolha anterior, e identificar as suas vantagens e desvantagens.
A solução atual baseada nas tecnologias da \textit{Microsoft} é suportada usando a plataforma \textit{Web}, colaborativa e corporativa, \textit{Sharepoint} em conjunto com serviços como \textit{Excel Services} ou \textit{Power View} que permite a criação de \textit{dashboards} e \textit{reports}.
Depois de uma análise sobre a tecnologia usada atualmente, foram identificados alguns problemas.

\begin{itemize}
\item Suporte e compatibilidade apenas com um \textit{browser}: \textit{Internet Explorer};
\item Incompatibilidade com dispositivos móveis;
\item Infrastrutura demasiado complexa para as organizações que apenas necessitam da partilha e edição de um conjunto de \textit{dashboards} e \textit{repors}.
\end{itemize}

Para tentar responder a este conjunto de requisitos e melhorar a solução existente, foi investigada a tecnologia mais recente orientada para o desenvolvimento de soluções de \textit{BI} : \textit{Power BI}.
Esta tecnologia foi desenvolvida e é comercializada pela \textit{Micrososft} e posicionou-se no mercado para tentar colmatar as dificuldades que até então existiam com as diversas plataformas existentes.
Analisando esta alternativa, facilmente reparamos que o \textit{Power BI} está preparado para ser utilizado nos dispositivos móveis e que a incompatibilidade com diferentes \textit{browsers} foi também ultrapassada. A infrastrutura que é necessária para utilizar o \textit{Power BI} também pode ser considerada uma vantagem devido a este serviço ser todo ele baseado na \textit{cloud}.

Ainda assim, reparando em todas as vantagens que o \textit{Power BI} trouxe com a sua chegada ao mercado,  ainda existem algumas desvantagens em comparação com a utilização do \textit{Sharepoint}

\begin{itemize}
\item Necessidade de ter os \textit{dashboards} e \textit{reports} na \textit{cloud} em detrimento de ter uma solução totalmente \textit{on premise};
\item A exploração da informação de forma mais detalhada através do \textit{Excel} ainda não é totalmente suportada;
\item Constante adaptação devido à evolução da solução; Neste momento o \textit{Power BI} continua em desenvolvimento e é necessária uma contínua aprendizagem para perceber as implicações das suas atualizações e desenvolvimentos na solução; Pode ser necessário dispender tempo que não estava inicialmente estimado.
\end{itemize}

Podemos assim concluir que as duas opções acabam por se completar, e por isso, para facilitar a integração na solução existente será utilizada a plataforma \textit{Sharepoint}, contudo, para perceber as potencialidades e responder a algumas necessidades específicas, como a compatabilidade com dispositivos móveis, será reproduzido o mesmo trabalho na plataforma \textit{Power BI}.


